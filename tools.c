#include "tools.h"

void print_img_rgba(png_bytep *img, int width, int height){
    for(int i=0; i < height; i++){
        for(int j=0; j < width; j++){
            printf("(");
            for(int k=0; k < 4; k++){
                printf("%3d, ", (unsigned char)(img[i][j*4+k]));
            }
            printf(") ");
        }
        printf("\n");
    }
}


void printn_img_rgba(png_bytep *img, int width, int height, int n){
    int count = 0;
    for(int i=0; i < height && count < n; i++){
        for(int j=0; j < width  && count < n; j++){
            printf("(");
            for(int k=0; k < 4  && count < n; k++){
                printf("%3d, ", (unsigned char)(img[i][j*4+k]));
                count++;
            }
            printf(") ");
        }
        printf("\n");
    }
}




/*open a png*/
png_bytep* open_png_read(char *file_name, png_structp *png_ptr, png_infop *info_ptr){
    
    FILE *fp = fopen(file_name, "rb");
    if (!fp){
        fprintf(stderr, "impossible d'ouvrir le fichier %s\n", file_name);
        return NULL;
    }
    
    uint8_t header[PNG_HEADER_LEN];
    fread(header, 1, PNG_HEADER_LEN, fp);
    
    if(png_sig_cmp(header, 0, PNG_HEADER_LEN)){
        fprintf(stderr, "le fichier %s n'est pas un png\n", file_name);
        return NULL;
    }
    
    
    *png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,
                                                 NULL, NULL, NULL);
    if (!png_ptr){
        fprintf(stderr, "impossible de créer struct png_structp\n");
        return NULL;
    }

    *info_ptr = png_create_info_struct(*png_ptr);
    if (!info_ptr)
    {
        fprintf(stderr, "impossible de créer struct png_infop\n");
        png_destroy_read_struct(png_ptr,
           (png_infopp)NULL, (png_infopp)NULL);
        return NULL;
    }
    
    png_init_io(*png_ptr, fp);
    png_set_sig_bytes(*png_ptr, PNG_HEADER_LEN);
    
    /*png_read_png(*png_ptr,
                 *info_ptr,
                 PNG_TRANSFORM_STRIP_16 || PNG_TRANSFORM_PACKING || PNG_TRANSFORM_EXPAND,
                 NULL);
    */
    
    png_read_info(*png_ptr, *info_ptr);
    
    int color_type = png_get_color_type(*png_ptr, *info_ptr);
    int bit_depth = png_get_bit_depth(*png_ptr, *info_ptr);
    
    
    if (color_type == PNG_COLOR_TYPE_PALETTE){
        png_set_palette_to_rgb(*png_ptr);
        printf("palette\n");
    }
    
    if (bit_depth < 8){
        printf(" < 8\n");
        png_set_expand(*png_ptr);
    }
    
    if(color_type == PNG_COLOR_TYPE_GRAY){
        printf("gray\n");
        png_set_gray_to_rgb(*png_ptr);
    }
    
    if (png_get_valid(*png_ptr, *info_ptr, PNG_INFO_tRNS)){
        printf("has alpha\n");
        png_set_tRNS_to_alpha(*png_ptr);
    }
    
    if (bit_depth == 16){
        printf("strip down\n");
        png_set_strip_16(*png_ptr);
    }
    
    int height = png_get_image_height(*png_ptr, *info_ptr);
    int width = png_get_image_width(*png_ptr, *info_ptr);
//    int row_lenght = png_get_rowbytes(*png_ptr, *info_ptr);
    
    png_bytep *row_pointers = malloc(sizeof(png_bytep)*height);
    for(int line=0; line < height; line++){
        row_pointers[line] = malloc(4*width);
    }
    png_set_filler(*png_ptr, 0xff, PNG_FILLER_AFTER);
    

    png_read_image(*png_ptr, row_pointers);
    png_read_end(*png_ptr, NULL);
    
    fclose(fp); // ??
    return row_pointers;
}


/*save a png file file_name with data of hints*/
int open_png_write(char *file_name, png_structp hints_png, png_infop hints_info, png_bytep *data){
    
    png_uint_32 width, height;
    int bit_depth, color_type, interlace_type, compression_type, filter_method;
    png_get_IHDR(hints_png,
                 hints_info,
                 &width, &height,
                 &bit_depth,
                 &color_type, 
                 &interlace_type,
                 &compression_type, 
                 &filter_method);
    
    FILE *dest_file = fopen(file_name, "wb");
    
    if (!dest_file){
        perror(file_name);
        return EXIT_FAILURE;
    }
       
    
    png_structp write_ptr = png_create_write_struct
       (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!write_ptr)
       return EXIT_FAILURE;

    png_infop info_write_ptr = png_create_info_struct(write_ptr);
    if (!info_write_ptr)
    {
       png_destroy_write_struct(&write_ptr,
         (png_infopp)NULL);
       return EXIT_FAILURE;
    }
    png_set_IHDR(write_ptr,
                 info_write_ptr,
                 width, height,
                 bit_depth, 
                 PNG_COLOR_TYPE_RGB_ALPHA, //provisoire, il faudrait changer les infos dans les struct au moment de read
                 interlace_type,
                 compression_type, 
                 filter_method);
    
    //png_bytep *row_pointers = // = png_get_rows(hints_png, hints_info);
    png_set_rows(write_ptr, info_write_ptr, data);
    
    png_init_io(write_ptr, dest_file);
    png_write_png(write_ptr, info_write_ptr, PNG_TRANSFORM_IDENTITY, NULL);
    
    
    png_destroy_write_struct(&write_ptr,
                            &info_write_ptr);
    
    fclose(dest_file);
    return EXIT_SUCCESS;
}


void write_LSD(uint8_t *data, int data_len, png_bytep *rows, int width, int height){
    int current_byte_ind = 0;
    int current_bit_ind = 7;
    
    if(data_len > width*height){
        fprintf(stderr, "not enough space in the base image\n");
        return;
    }
    
    
    bool write_complete = false;
    png_byte* px;
    for(int y=0; y < height && !write_complete; y++){

        for(int x=0; x < width && !write_complete; x++){
            px = &(rows[y][x*4]);
            
            for(int i=0; i < 4 && !write_complete; i++){
                if(current_bit_ind < 0){
                    current_byte_ind++;

                    current_bit_ind = 7;
                }
                if(current_byte_ind >= data_len){
                    write_complete = true;
                }else{
                    px[i] >>= 1;
                    px[i] <<= 1;
                    px[i] += (data[current_byte_ind]>>current_bit_ind)&1;
                    current_bit_ind--;
                }
            }
        }
    }
}


void read_LSD(uint8_t *data, int data_len, png_bytep *rows, int width, int height){
    int current_byte_ind = 0;
    int current_bit_ind = 0;
    
    bool read_complete = false;
    png_bytep px;
    for(int y=0; y < height && !read_complete; y++){
        for(int x=0; x < width && !read_complete; x++){
            px = &(rows[y][x*4]);
            for(int i=0; i < 4 && !read_complete; i++){
                if(current_bit_ind >= 8){

                    current_byte_ind++;
                    current_bit_ind = 0;
                }
                if(current_byte_ind >= data_len){
                    read_complete = true;
                }else{
                    data[current_byte_ind] <<= 1;
                    data[current_byte_ind] += px[i]&1;
                    current_bit_ind++;
                }

            }
        }
    }
}

DATA_TYPES get_type(uint8_t* data, int len){

    if(png_sig_cmp(data, 0, PNG_HEADER_LEN) == 0){
        return PNG;
        
    }else{
        bool found = false;
        for(int i=0; i < len && !found; i++){
            if(data[i] == '\0'){
                found = true;
            }
        }
        
        if(found){
            return STR;
        }
    }
    
    return -1;
}



unsigned char* lookup(const unsigned char*seq, int seq_len, const unsigned char* src, int src_len){
    bool matching = false;

    int i;
    for(i=src_len-1; i >= 0 && !matching; i--){
        
        if(src[i] == seq[seq_len-1]){
            matching = true;
            for(int j=0; j < seq_len && matching; j++){
                matching = (seq[j] == src[i-(seq_len-1-j)]);
            }
        }
    }
    
    return matching ? (unsigned char*)(src+i-(seq_len-1)+1) : NULL;
}
