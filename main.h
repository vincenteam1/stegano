#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <png.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "tools.h"

#define PNG_HEADER_LEN 8
