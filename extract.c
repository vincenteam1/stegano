#include "extract.h" 


int main(int argc, char *argv[]){
    if(argc < 2){
        printf("usage: extract <file name>\n");
        return EXIT_FAILURE;
    }
    
    png_structp png_base_ptr;
    png_infop info_base_ptr;
    png_bytep *img_base_data = open_png_read(argv[1], &png_base_ptr, &info_base_ptr);
    
    
    int base_width, base_height;
    base_width = png_get_image_width(png_base_ptr, info_base_ptr);
    base_height = png_get_image_height(png_base_ptr, info_base_ptr);
    
    int max_data_len = (base_width*base_height*4)/8;
    uint8_t *message_in = calloc(max_data_len, sizeof(uint8_t));
    read_LSD((unsigned char*)message_in, max_data_len, img_base_data, base_width, base_height);
    
    
    switch(get_type(message_in, max_data_len)){
        case STR:
            printf("string found:\n");
            printf("%s\n", message_in);
            // sauver dans un .txt
            break;
        case PNG:
            
            unsigned char* img_end = lookup(png_end, 8, message_in, max_data_len);
            if(img_end == NULL){
                fprintf(stderr, "png start found but no png end\n");
                break;
            }
            
            printf("png found\n");
            FILE* outf = fopen("hidden_image.png", "w+");
            
            img_end += PNG_END_LENGHT;
            
            int img_len = img_end - message_in;
            
            for(int i=0; i < img_len; i++){
                fputc(message_in[i], outf);
            }
            
            fclose(outf);
            
            break;
        default:
            fprintf(stderr, "unrecognized data format\n");
            break;
    }
    
    free(message_in);
    
    return EXIT_SUCCESS;
}
