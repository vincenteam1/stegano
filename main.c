#include "main.h"


int main(int argc, char *argv[]){
    
    if(argc < 3){
        printf("les arguments ^^\n<base> <data>\n");
        return EXIT_FAILURE;
    }
    
    png_structp png_base_ptr;
    png_infop info_base_ptr;
    png_bytep *img_base_data = open_png_read(argv[1], &png_base_ptr, &info_base_ptr);
    
    png_uint_32 base_width, base_height;
    base_width = png_get_image_width(png_base_ptr, info_base_ptr);
    base_height = png_get_image_height(png_base_ptr, info_base_ptr);
    
    
    /*switch(png_get_color_type(png_base_ptr, info_base_ptr)){
        case PNG_COLOR_TYPE_GRAY:
            printf("PNG_COLOR_TYPE_GRAY");
            break;
        case PNG_COLOR_TYPE_GRAY_ALPHA:
            printf("PNG_COLOR_TYPE_GRAY_ALPHA");
            break;         
        case PNG_COLOR_TYPE_PALETTE:
            printf("PNG_COLOR_TYPE_PALETTE");
            break;
        case PNG_COLOR_TYPE_RGB:
            printf("PNG_COLOR_TYPE_RGB");
            break;
        case PNG_COLOR_TYPE_RGB_ALPHA:
            printf("PNG_COLOR_TYPE_RGB_ALPHA");
            break;
        default:
            printf("color type unknown");
            break;
    }
    printf("\n");
    printf("depth : %d\n", png_get_bit_depth(png_base_ptr, info_base_ptr));
    printf("rowsize : %ld", png_get_rowbytes(png_base_ptr, info_base_ptr));
    printf("\n");*/
    //////////////////////////
    
    FILE* contentf = fopen(argv[2], "r");
    
    struct stat content_info;
    stat(argv[2], &content_info);
    int len = content_info.st_size;
    
    uint8_t* data = malloc(len * sizeof(uint8_t));
    
    int c;
    int i = 0;
    while((c = fgetc(contentf)) != EOF){
        data[i++] = (char)c;
    }
    
    
    write_LSD(data, len, img_base_data, base_width, base_height);
    
    open_png_write("out.png", png_base_ptr, info_base_ptr, img_base_data);

    png_destroy_read_struct(&png_base_ptr,
                            &info_base_ptr,
                            NULL);

    return EXIT_SUCCESS;
}
