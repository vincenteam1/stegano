# Stéganographie sur des png en c

Ce programme implémente une méthode de stéganographie (dissimulation d'information) avec des images.

Il permet de dissimuler une suite d'octets (on utilisera les données d'une image, mais n'importe quelle donnée peut être utilisée) dans une image, en changeant uniquement le dernier bit de chaque octet encodant la couleur des pixels, la modification visuelle est donc minime (invisible à l'œil nu) et l'information parfaitement conservée. Cela est possible uniquement car le format png utilise des algorithmes de compression sans perte.


## Exemple d'utilisation

compilation:
```
make
```

inscription de donnée dans une images

```bash
./main images/z.png images/zircon.png
```
une image `out.png` est crée, elle contient l'image z.png modifiée pour contenir les données de zircon.png


extraction des données
```
./extract out.png
```

un fichier `hidden_image.png` est créé à partir de out.png, il est identique au fichier de départ `zircon.png`