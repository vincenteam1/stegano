all: main extract

main : tools main.c main.h
	gcc -Wall -o main main.c tools.o -lpng

extract : tools extract.c extract.h
	gcc -Wall -o extract extract.c tools.o -lpng
	
test : tools test.c test.h
	gcc -Wall -o stegano_test test.c tools.o -lpng
	
tools: tools.c tools.h
	gcc -Wall -c tools.c
