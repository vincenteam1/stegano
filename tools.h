#ifndef TOOLS
#define TOOLS

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <png.h>

#define PNG_HEADER_LEN 8

enum DATA_TYPES {STR, PNG};
typedef enum DATA_TYPES DATA_TYPES;

void read_LSD(uint8_t *data, int data_len, png_bytep *rows, int width, int height);
void write_LSD(uint8_t *data, int data_len, png_bytep *rows, int width, int height);
int open_png_write(char *file_name, png_structp hints_png, png_infop hints_info, png_bytep *data);
png_bytep* open_png_read(char *file_name, png_structp *png_ptr, png_infop *info_ptr);
void printn_img_rgba(png_bytep *img, int width, int height, int n);
void print_img_rgba(png_bytep *img, int width, int height);
DATA_TYPES get_type(uint8_t* data, int len);
unsigned char* lookup(const unsigned char*seq, int seq_len, const unsigned char* src, int src_len);

#endif
